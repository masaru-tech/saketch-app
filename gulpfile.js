var gulp = require('gulp');
var webpack = require('gulp-webpack');
var webpackConfig = require('./webpack.config.js');
var rename = require("gulp-rename");
var replace = require('gulp-replace');

gulp.task('build', function () {
  return gulp.src('')
  .pipe(webpack(webpackConfig))
  .pipe(gulp.dest('app/assets/javascripts/components/'));
});

gulp.task('onsenui', ['copy-onsen-css', 'copy-onsen-js', 'copy-onsen-font']);

gulp.task('copy-onsen-css', function(){
  return gulp.src([
    'node_modules/onsenui/css/onsenui.css',
    'node_modules/onsenui/css/onsen-css-components-blue-theme.css',
    'node_modules/onsenui/css/font_awesome/css/font-awesome.css',
    'node_modules/onsenui/css/ionicons/css/ionicons.css',
    'node_modules/onsenui/css/material-design-iconic-font/css/material-design-iconic-font.css'
  ])
  .pipe(rename({
      extname: '.css.scss'
    }))
  .pipe(replace("url", 'font-url'))
  .pipe(replace(/background-image: font-url(.*)/, "background-image: url$1"))
  .pipe(replace("../fonts/", ""))
  .pipe(replace(/(@import .*)/g, ""))
  .pipe(gulp.dest('app/assets/stylesheets/'));
});

gulp.task('copy-onsen-js', function(){
  return gulp.src([
    'node_modules/onsenui/js/onsenui.js'
  ])
  .pipe(gulp.dest('app/assets/javascripts/'));
});

gulp.task('copy-onsen-font', function(){
  return gulp.src([
    'node_modules/onsenui/css/font_awesome/fonts/*',
    'node_modules/onsenui/css/ionicons/fonts/*',
    'node_modules/onsenui/css/material-design-iconic-font/fonts/*'
  ])
  .pipe(gulp.dest('app/assets/fonts/'))
});

gulp.task('default', ['build', 'onsenui']);
