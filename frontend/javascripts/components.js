import { Parse } from 'parse';
Parse.initialize('id', 'key');
window.Parse = Parse;
window.LoginContent = require('./components/login.jsx').default;
window.NewUser = require('./components/new_user.jsx').default;
window.ResetPassword = require('./components/reset_password.jsx').default;
window.SakeListContent = require('./components/sake_list.jsx').default;
window.SakeDetailContent = require('./components/sake_detail.jsx').default;
window.SakeEvaluationContent = require('./components/sake_evaluation.jsx').default;
