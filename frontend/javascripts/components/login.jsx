import Header from './header.jsx'

var Link = ReactRouter.Link

class LoginContent extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      username: "",
      password: ""
    };
  }

  onClick() {
    self = this;
    Parse.User.logIn(this.state.username, this.state.password)
      .then(function() {
        var user = Parse.User.current();
        if (user && user.get("emailVerified")) {
          self.context.router.transitionTo('/sakes/index');
        }
      }, function(error) {
        console.log(error);
      });
  }

  render() {
    return (
      <div className="page">
        <Header title="ログイン" back="true" />
        <div style={{paddingTop: "44px"}}>
          <div className="login-form">
            <input type="text" className="text-input--underbar" name="user[username]" placeholder="ユーザ名" valueLink={this.linkState('username')} />
            <input type="password" className="text-input--underbar" name="user[password]" placeholder="パスワード" valueLink={this.linkState('password')} />
            <br /><br />
            <button className="button login-button button--large" onClick={this.onClick.bind(this)}>ログイン</button>
            <br /><br />
            <Link to="reset_password">パスワードを忘れた場合</Link>
          </div>
        </div>
      </div>
    );
  }
}

LoginContent.prototype.linkState = React.addons.LinkedStateMixin.linkState

LoginContent.contextTypes = {
  router: function contextType() {
    return React.PropTypes.func.isRequired;
  }
};

export default LoginContent
