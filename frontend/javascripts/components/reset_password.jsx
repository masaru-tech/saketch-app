import Header from './header.jsx'

class ResetPassword extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      email: "",
    };
  }
  render() {
    return (
      <div className="page">
        <Header title="パスワードリセット" back="true" />
        <div style={{paddingTop: "44px"}}>
          <div className="login-form">
            <input type="email" className="text-input--underbar" name="user[email]" placeholder="メールアドレス" valueLink={this.linkState('email')} />
            <br /><br />
            <button className="button login-button button--large">パスワードをリセットする</button>
          </div>
        </div>
      </div>
    );
  }
}

ResetPassword.prototype.linkState = React.addons.LinkedStateMixin.linkState

export default ResetPassword
