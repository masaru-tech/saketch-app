import React from "react";

class Icon extends React.Component {
  render() {
    var iStyle = {
      cursor: 'pointer'
    };
    var className = this.props.toggled ? this.props.toggledClassName : this.props.untoggledClassName;
    return (
      <i className={className} onMouseMove={this.props.onMouseEnter} style={iStyle} onClick={this.props.onClickRating}/>
    );
  }
}

class IconRating extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      currentRating: this.props.currentRating || 0,
      max: this.props.max || 5,
      currentRating_hover: 0,
      hovering: false
    };
    this.onMouseEnter = this.onMouseEnter.bind(this);
    this.onMouseLeave = this.onMouseLeave.bind(this);
  }

  onMouseEnter(currentRating, e, id) {
    var rating = currentRating;
    if((e.nativeEvent.clientX) < (e.target.offsetLeft + (e.target.offsetWidth / 2))){
      rating -= .5;
    }
    this.setState({
      currentRating_hover: rating,
      hovering: true
    });
  }

  onMouseLeave(currentRating, e, id) {
    this.setState({
      hovering: false
    });
  }

  onClickRating(currentRating, e, id) {
    this.setState({
      currentRating: currentRating
    });
    if(this.props.onChange){
      this.props.onChange(currentRating);
    }
  }

  render() {
    var ratings = [];
    var toggled = false, rating, halfClassName,
        f = ()=>{},
        onMouseEnter = this.props.viewOnly ? f : this.onMouseEnter,
        onClickRating = this.props.viewOnly ? f : this.onClickRating;
    for(let i=1;i<=this.state.max;++i){
      rating = this.state['currentRating' + (this.state.hovering ? '_hover':'')];
      toggled = i <= Math.round(rating) ? true : false;
      halfClassName = null;
      if(this.props.halfClassName &&
         Math.round(rating) == i &&
         Math.floor(rating) != rating){
          halfClassName = this.props.halfClassName;
      }
      ratings.push(
          <Icon key={i} toggledClassName={halfClassName || this.props.toggledClassName} untoggledClassName={this.props.untoggledClassName} onMouseEnter={onMouseEnter.bind(this,i)} onClickRating={onClickRating.bind(this,i)} toggled={toggled}/>
      );
    }
    return (
      <div className={this.props.className} onMouseLeave={this.onMouseLeave}>
        {ratings}
      </div>
    );
  }
}

export default IconRating;
