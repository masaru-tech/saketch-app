import Header from './header.jsx'
import Evaluation from "./evaluation-tools/evaluation.jsx";
import axios from 'axios'

class SakeImage extends React.Component {
  render() {
    var style = {};
    if (this.props.url) {
      var bgImage = 'url("' + this.props.url + '")';
      var style = {backgroundImage: bgImage};
    }
    return (
      <div className="card" style={style}>
      </div>
    );
  }
}

class SakeRating extends React.Component {
  render() {
    var ratingIcons = [];
    var rating = this.props.rating;
    for(let i=1;i<=5;++i){
      var className = null;
      if(i <= rating) {
        className = "icomoon-heart";
      }
      if(Math.round(rating) == i &&
         Math.floor(rating) != rating){
          className = "icomoon-heart-half";
      }
      if (className) {
        ratingIcons.push(
          <span key={i} className={className}></span>
        );
      }
    }
    return (
      <div className="rating">
        <div className="rating-num">
          {ratingIcons}
          {rating || "-----"}
        </div>
      </div>
    );
  }
}
SakeRating.defaultProps = {
  rating: null
};

class SakeActionButtons extends React.Component {
  render() {
    return (
      <ul className="list" style={{borderTop: 'none'}}>
        <li key="sake-action-buttons" className="list__item" style={{lineHeight: '1', padding: '0'}}>
          <ons-row className="action" style={{"padding": "5px 0px"}}>
            <SakeWantButton id={this.props.id} />
            <SakeDrunkButton id={this.props.id} />
          </ons-row>
        </li>
      </ul>
    );
  }
}

class SakeWantButton extends React.Component {
  render() {
    return (
      <ons-col className="action-col" style={{"textAlign": "center"}}>
        <button className="button">
          <span className="icomoon-heart"></span>
          呑みたい！
        </button>
      </ons-col>
    );
  }
}

class SakeDrunkButton extends React.Component {
  moveEvaluation() {
    var url = '/sakes/:id/evaluation';
    this.context.router.transitionTo(url, {id: this.props.id});
  }

  render() {
    return (
      <ons-col className="action-col" style={{"textAlign": "center"}}>
        <button className="button" onClick={this.moveEvaluation.bind(this)}>
          <span className="fa fa-pencil-square-o"></span>
          呑んだ！
        </button>
      </ons-col>
    );
  }
}
SakeDrunkButton.contextTypes = {
  router: function contextType() {
    return React.PropTypes.func.isRequired;
  }
};

class SakeInfos extends React.Component {
  constructor (props) {
    super(props);
    this.onChange = this.onChange.bind(this);
  }

  onChange(value) {}

  render() {
    if (this.props.sake.cleaning_rate.min == this.props.sake.cleaning_rate.max) {
      if (this.props.sake.cleaning_rate.min != null) {
        var cleaning_rate = this.props.sake.cleaning_rate.min + "%";
      }
    } else {
      var cleaning_rate = this.props.sake.cleaning_rate.min + "〜" + this.props.sake.cleaning_rate.max + "%";
    }

    if (this.props.sake.nihonsyu_degree.min == this.props.sake.nihonsyu_degree.max) {
      if (this.props.sake.nihonsyu_degree.min != null) {
        var nihonsyu_degree = this.props.sake.nihonsyu_degree.min;
      }
    } else {
      var nihonsyu_degree = this.props.sake.nihonsyu_degree.min + "〜" + this.props.sake.nihonsyu_degree.max;
    }

    if (this.props.sake.alcohol_content.min == this.props.sake.alcohol_content.max) {
      if (this.props.sake.alcohol_content.min != null) {
        var alcohol_content = this.props.sake.alcohol_content.min + "%";
      }
    } else {
      var alcohol_content = this.props.sake.alcohol_content.min + "〜" + this.props.sake.alcohol_content.max + "%";
    }

    return (
      <ul className="list">
        <li className="list__header">お酒詳細</li>
        <li className="list__item">
          <table className="sake-detail">
            <tbody>
              <tr>
                <td width="30%">銘柄</td>
                <td width="70%">{this.props.sake.name}</td>
              </tr>
              <tr>
                <td>読み</td>
                <td>{this.props.sake.yomi}</td>
              </tr>
              <tr>
                <td>種類</td>
                <td>{this.props.sake.category.join(", ")}</td>
              </tr>
              <tr>
                <td>蔵元</td>
                <td>{this.props.sake.kuramoto}</td>
              </tr>
              <tr>
                <td>都道府県</td>
                <td>{this.props.sake.prefecture}</td>
              </tr>
              <tr>
                <td>原料米</td>
                <td>{this.props.sake.raw_rice.join(", ")}</td>
              </tr>
              <tr>
                <td>こうじ</td>
                <td>{this.props.sake.use_east}</td>
              </tr>
              <tr>
                <td>精米歩合</td>
                <td>{cleaning_rate}</td>
              </tr>
              <tr>
                <td>アルコール度</td>
                <td>{alcohol_content}</td>
              </tr>
              <tr>
                <td>日本酒度</td>
                <td>{nihonsyu_degree}</td>
              </tr>
              <tr>
                <td>酸度</td>
                <td>{this.props.sake.acidity}</td>
              </tr>
              <tr>
                <td>アミノ酸度</td>
                <td>{this.props.sake.acidity}</td>
              </tr>
              <tr>
                <td>備考</td>
                <td>{this.props.sake.note}</td>
              </tr>
            </tbody>
          </table>
        </li>
        <li className="list__header">評価</li>
        <li className="list__item">
          <Evaluation
            value={this.props.value}
            onChange={this.onChange}
            style={{
              display: "inline-block",
              margin: "5px"
            }}
            handleStroke={3}
            handleRadius={10}
            readOnly>
          </Evaluation>
        </li>
      </ul>
    );
  }
}

class SakeDetailContent extends React.Component {
  constructor(props) {
    super(props);
    var sake = {
      id: "",
      name: "",
      yomi: "",
      category: [],
      prefecture: "",
      kuramoto: "",
      raw_rice: [],
      cleaning_rate: {
        min: "",
        max: ""
      },
      nihonsyu_degree: {
        min: "",
        max: ""
      },
      amino_degree: "",
      acidity: "",
      alcohol_content: {
        min: "",
        max: ""
      },
      use_east: "",
      note: ""
    };

    if (props.sake) {
      sake = props.sake;
    }

    this.state = {
      value: [ -1, -1 ],
      rating: null,
      sake: sake,
      url: null
    };
  }

  componentDidMount() {
    var self = this;
    var image_query = new Parse.Query(Parse.Object.extend('SakeImage'));
    image_query.equalTo("sakeId", this.props.params.id);

    Promise.all([
      axios.get('/api/v1/sakes/' + this.props.params.id),
      image_query.first()
    ])
    .then((results) => {
      self.setState({sake: results[0].data});
      var sakeImage = results[1];
      if (sakeImage) {
        self.setState({
          url: sakeImage.get('image').url()
        })
      }
    },
    (error) => {
      console.log(error);
    });

    if (Parse.User.current()) {
      var eval_query = new Parse.Query(Parse.Object.extend('Evaluation'));
      eval_query.equalTo("sakeId", this.props.params.id);
      eval_query.equalTo("user", Parse.User.current());
      eval_query.first()
        .then((evaluation)=> {
          if (evaluation) {
            self.setState({
              value: evaluation.get('evaluation'),
              rating: evaluation.get('rating')
            });
          }
        },
        (error) => {
          console.log(error);
        });
    }
  }

  render() {
    return (
      <div className="page">
        <Header title="詳細" back="true" />
        <div style={{paddingTop: "44px"}}>
          <SakeImage url={this.state.url} />
          <SakeRating rating={this.state.rating} />
          <SakeActionButtons id={this.props.params.id} />
          <SakeInfos sake={this.state.sake} value={this.state.value} />
        </div>
      </div>
    );
  }
}

export default SakeDetailContent
