'use strict';

class Header extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      sign_in: undefined
    }
    this.signOutHandler = this.signOutHandler.bind(this);
  }

  signOutHandler(status) {
    this.setState({sign_in: status});
  }

  componentDidMount() {
    var user = Parse.User.current();
    this.setState({
      sign_in: !!user && user.get("emailVerified")
    });
  }

  render() {
    if (this.props.back) {
      var backicon = <HeaderIcon iconname='fa-chevron-left' />;
    }

    if (this.state.sign_in != undefined) {
      if (this.props.login && !this.state.sign_in) {
        var loginicon = <LoginButton />;
      }

      if (this.props.login && this.state.sign_in) {
        var logouticon = <LogoutButton signOutHandler={this.signOutHandler} />;
      }
    }

    return (
      <div className="navigation-bar">
        <div className="navigation-bar__left">
          {backicon}
        </div>

        <div className="navigation-bar__center">
          {this.props.title}
        </div>

        <div className="navigation-bar__right">
          {loginicon}
          {logouticon}
        </div>
      </div>
    );
  }
}
Header.DefaultProps = {
  back: false,
  login: false,
};

class HeaderIcon extends React.Component {
  _goBack(e) {
    this.context.router.goBack();
  }

  render() {
    var name = "fa " + this.props.iconname;
    return (
      <span className="toolbar-button navigation-bar__line-height" onClick={this._goBack.bind(this)}>
        <i className={name}></i>
      </span>
    );
  }
}
HeaderIcon.contextTypes = {
  router: function contextType() {
    return React.PropTypes.func.isRequired;
  }
};

class LoginButton extends React.Component {
  loginPage() {
    this.context.router.transitionTo('/users/sign_in')
  }

  render() {
    return (
      <span className="toolbar-button navigation-bar__line-height" onClick={this.loginPage.bind(this)}>
        <i className="fa fa-sign-in"></i>
      </span>
    );
  }
}
LoginButton.contextTypes = {
  router: function contextType() {
    return React.PropTypes.func.isRequired;
  }
};

class LogoutButton extends React.Component {
  logout() {
    var self = this;
    Parse.User.logOut()
      .then(function (response) {
        self.props.signOutHandler(false);
      }, function(error) {
        console.log(error);
      });
  }

  render() {
    return (
      <span className="toolbar-button navigation-bar__line-height" onClick={this.logout.bind(this)}>
        <i className="fa fa-sign-out"></i>
      </span>
    );
  }
}

export default Header
