import Header from './header.jsx'
import VirtualList from 'react-virtual-list'
import axios from 'axios'

class SakeList extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      sakes: props.sakes
    };
    this.onChange = this.onChange.bind(this);
    this.renderItem = this.renderItem.bind(this);
  }

  componentWillReceiveProps(props) {
    this.setState({
      sakes: props.sakes
    });
  }

  onChange(e) {
    if (e.target.value == "") {
      this.setState({sakes: this.props.sakes});
    } else {
      var reg = new RegExp(e.target.value, "i");
      this.setState({sakes: this.props.sakes.filter((sake) => {
        return sake.name.match(reg) || sake.yomi.match(reg)
      })});
    }
  }

  renderItem(sake) {
    return (
        <SakeListItem key={sake.id} sake={sake} />
    );
  }

  render() {
    var opts = {
      itemHeight: 55,
      itemBuffer: 10
    };
    return(
      <div style={{paddingTop: "44px"}}>
        <div style={{width: "100%"}}>
          <input type="search" className="search-input" style={{width: "96%", margin: "6px auto 6px auto"}} placeholder="お酒検索" onChange={this.onChange} />
        </div>
        <VirtualList items={this.state.sakes} tagName="ul" className="list" itemBuffer={opts.itemBuffer} renderItem={this.renderItem} itemHeight={opts.itemHeight}  />
      </div>
    );
  }
}

class SakeListItem extends React.Component {
  _onShow() {
    var url = '/sakes/:id/show';
    this.context.router.transitionTo(url, {id: this.props.sake.id});
  }
  render() {
    return (
        <li key={this.props.sake.id} className="list__item list__item--chevron list__item--tappable" onClick={this._onShow.bind(this)}>
          <div className="sake-main">{this.props.sake.name}</div>
          <div className="sake-sub">{this.props.sake.kuramoto} / {this.props.sake.prefecture}</div>
        </li>
    );
  }
}
SakeListItem.contextTypes = {
  router: function contextType() {
    return React.PropTypes.func.isRequired;
  }
};


class SakeListContent extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      sakes: props.sakes
    };
  }

  componentDidMount() {
    var self = this;
    axios.get('/api/v1/sakes')
    .then(function (response) {
      self.setState({sakes: response.data.sakes});
    })
    .catch(function (error) {
      console.log(error);
    });
  }

  render() {
    return (
      <div className="page">
        <Header title="お酒" login="true" />
        <SakeList sakes={this.state.sakes} />
      </div>
    );
  }
};
SakeListContent.defaultProps = {
  sakes: []
};

export default SakeListContent
