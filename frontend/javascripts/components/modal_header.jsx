'use strict';

class ModalHeader extends React.Component {
  render() {
    return (
      <div className="navigation-bar">
        <div className="navigation-bar__left">
          <CancelButton />
        </div>

        <div className="navigation-bar__center">
          {this.props.title}
        </div>

        <div className="navigation-bar__right">
          <RegisterButton registerHandler={this.props.registerHandler} />
        </div>
      </div>
    );
  }
}

class RegisterButton extends React.Component {
  onClick() {
    var self = this;
    if (this.props.registerHandler) {
      this.props.registerHandler()
          .then(function (response) {
            self.context.router.goBack();
          }, function (error) {
            console.log(error);
          });
    }
  }

  render() {
    return (
      <span className="toolbar-button navigation-bar__line-height" onClick={this.onClick.bind(this)}>
        登録
      </span>
    );
  }
}
RegisterButton.contextTypes = {
  router: function contextType() {
    return React.PropTypes.func.isRequired;
  }
};

class CancelButton extends React.Component {
  onClick() {
    this.context.router.goBack();
  }

  render() {
    return (
      <span className="toolbar-button navigation-bar__line-height" onClick={this.onClick.bind(this)}>
        キャンセル
      </span>
    );
  }
}
CancelButton.contextTypes = {
  router: function contextType() {
    return React.PropTypes.func.isRequired;
  }
};

export default ModalHeader
