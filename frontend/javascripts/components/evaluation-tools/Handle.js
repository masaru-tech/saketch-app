import React from "react";

function interp (a, b, x) {
  return a * (1-x) + b * x;
}

export default class Handle extends React.Component {
  constructor (props) {
    super(props);
    this.x = this.x.bind(this);
    this.y = this.y.bind(this);
  }

  x (value) {
    return Math.round(interp(this.props.xFrom, this.props.xTo, value));
  }

  y (value) {
    return Math.round(interp(this.props.yFrom, this.props.yTo, value));
  }

  shouldComponentUpdate(nextProps) {
    const {
      xFrom,
      yFrom,
      xTo,
      yTo
    } = this.props;
    if (nextProps.xFrom !== xFrom ||
      nextProps.yFrom !== yFrom ||
      nextProps.xTo !== xTo ||
      nextProps.yTo !== yTo) {
        return true;
      }

    const {
      index,
      handleRadius,
      handleColor,
      hover,
      down,
      background,
      handleStroke,
      xval,
      yval,
      onMouseEnter,
      onMouseLeave,
      onMouseDown
    } = this.props;
    return nextProps.index !== index ||
    nextProps.handleRadius !== handleRadius ||
    nextProps.handleColor !== handleColor ||
    nextProps.hover !== hover ||
    nextProps.down !== down ||
    nextProps.background !== background ||
    nextProps.handleStroke !== handleStroke ||
    nextProps.xval !== xval ||
    nextProps.yval !== yval ||
    nextProps.onMouseDown !== onMouseDown ||
    nextProps.onMouseLeave !== onMouseLeave ||
    nextProps.onMouseEnter !== onMouseEnter;
  }

  render() {
    const { x, y } = this;
    const {
      index,
      handleRadius,
      handleColor,
      hover,
      down,
      background,
      handleStroke,
      xval,
      yval,
      onMouseEnter,
      onMouseLeave,
      onMouseDown
    } = this.props;

    const cx = x(xval);
    const cy = y(yval);

    return (
      <g>
        <circle
        cx={cx}
        cy={cy}
        r={handleRadius}
        stroke={handleColor}
        strokeWidth={hover||down ? 2 * handleStroke : handleStroke}
        fill={down ? background: handleColor}
        onMouseEnter={onMouseEnter}
        onMouseLeave={onMouseLeave}
        onMouseDown={onMouseDown}
        onTouchMove={onMouseLeave}
        onTouchStart={onMouseDown} />
      </g>
    )
  }
}
