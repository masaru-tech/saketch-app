import React from "react";
import ReactDOM from "react-dom";
import objectAssign from "object-assign";
const {PropTypes, Component} = React;

import Grid from "./Grid";
import Handle from "./Handle";

const propTypes = {
  value: PropTypes.array,
  onChange: PropTypes.func,
  width: PropTypes.number,
  height: PropTypes.number,
  padding: PropTypes.array,
  handleRadius: PropTypes.number,
  style: PropTypes.object,
  handleStroke: PropTypes.number,
  background: PropTypes.string,
  gridColor: PropTypes.string,
  handleColor: PropTypes.string,
  color: PropTypes.string,
  textStyle: PropTypes.object,
  readOnly: PropTypes.bool
};

const defaultProps = {
  value: [0.25, 0.25, 0.75, 0.75 ],
  width: 300,
  height: 300,
  padding: [25, 18, 25, 18],
  background: "#fff",
  color: "#000", // FIXME what is color?
  gridColor: "#eee",
  handleColor: "#f00",
  handleRadius: 5,
  handleStroke: 2,
  textStyle: {
    fontFamily: "sans-serif",
    fontSize: "10px"
  },
  pointers: {
    down: "none",
    hover: "pointer",
    def: "default"
  }
};

export default class Evaluation extends Component {

  constructor (props) {
    super(props);
    this.state = {
      down: 0,
      hover: 0
    };
    this.x = this.x.bind(this);
    this.y = this.y.bind(this);
    this.onDownLeave = this.onDownLeave.bind(this);
    this.onDownMove = this.onDownMove.bind(this);
    this.onDownUp = this.onDownUp.bind(this);
    this.onClick = this.onClick.bind(this);
    this.onEnterHandle = this.onEnterHandle.bind(this, 1);
    this.onLeaveHandle = this.onLeaveHandle.bind(this, 1);
    this.onDownHandle = this.onDownHandle.bind(this, 1);
  }

  render () {
    const { x, y } = this;
    const {
      value,
      width,
      height,
      handleRadius,
      style,
      handleStroke,
      background,
      gridColor,
      handleColor,
      textStyle,
      readOnly,
      pointers
    } = this.props;

    const {
      down,
      hover
    } = this.state;

    const sharedProps = {
      xFrom: x(0),
      yFrom: y(0),
      xTo: x(1),
      yTo: y(1)
    };

    const cursor = objectAssign({}, propTypes.pointers, pointers);

    const styles = objectAssign({
      background: background,
      cursor: down ? cursor.down : hover ? cursor.hover : cursor.def,
      userSelect: "none",
      WebkitUserSelect: "none",
      MozUserSelect: "none"
    }, style);

    const containerEvents = readOnly||!down ? {} : {
      onMouseMove: this.onDownMove,
      onMouseUp: this.onDownUp,
      onMouseLeave: this.onDownLeave,
      onTouchMove: this.onDownMove,
      onTouchEnd: this.onDownUp
    };
    const handleEvents = readOnly||down ? {} : {
      onMouseDown: this.onDownHandle,
      onMouseEnter: this.onEnterHandle,
      onMouseLeave: this.onLeaveHandle
    };

    return <svg
      style={styles}
      width={width}
      height={height}
      onClick={this.onClick}
      {...containerEvents}>
      <Grid {...sharedProps} background={background} gridColor={gridColor} textStyle={objectAssign({}, defaultProps.textStyle, textStyle)} />
      {this.props.children}
      <g>
        <Handle {...sharedProps} {...handleEvents} index={0} xval={value[0]} yval={value[1]} handleRadius={handleRadius} handleColor={handleColor} down={down===1} hover={hover===1} handleStroke={handleStroke} background={background} />
      </g>
    </svg>;
  }

  onClick (e) {
    e.preventDefault();
    const value = [].concat(this.props.value);
    const [ x, y ] = this.positionForEvent(e);
    value[0] = this.inversex(x);
    value[1] = this.inversey(y);
    this.props.onChange(value);
  }

  onDownLeave (e) {
    if (this.state.down) {
      this.onDownMove(e);
      this.setState({
        down: null
      });
    }
  }

  onDownHandle (h, e) {
    e.preventDefault();
    this.setState({
      hover: null,
      down: h
    });
  }

  onEnterHandle (h) {
    if (!this.state.down) {
      this.setState({
        hover: h
      });
    }
  }

  onLeaveHandle () {
    if (!this.state.down) {
      this.setState({
        hover: null
      });
    }
  }

  onDownMove (e) {
    if (this.state.down) {
      e.preventDefault();
      const i = 2*(this.state.down-1);
      const value = [].concat(this.props.value);
      const [ x, y ] = this.positionForEvent(e);
      value[i] = this.inversex(x);
      value[i+1] = this.inversey(y);
      this.props.onChange(value);
    }
  }

  onDownUp () {
    // this.onDownMove(e);
    this.setState({
      down: 0
    });
  }

  positionForEvent (e) {
    const rect = ReactDOM.findDOMNode(this).getBoundingClientRect();
    if (e.touches){
      return [
        e.touches[0].clientX - rect.left,
        e.touches[0].clientY - rect.top
      ];
    } else {
      return [
        e.clientX - rect.left,
        e.clientY - rect.top
      ];
    }
  }

  x (value) {
    const padding = this.props.padding;
    const w = this.props.width - padding[1] - padding[3];
    return Math.round(padding[3] + value * w);
  }

  inversex (x) {
    const padding = this.props.padding;
    const w = this.props.width - padding[1] - padding[3];
    return Math.max(0, Math.min((x-padding[3]) / w, 1));
  }

  y (value) {
    const padding = this.props.padding;
    const h = this.props.height - padding[0] - padding[2];
    return Math.round(padding[0] + (1-value) * h);
  }

  inversey (y) {
    const {
      height,
      handleRadius,
      padding
    } = this.props;
    const clampMargin = 2 * handleRadius;
    const h = height - padding[0] - padding[2];
    y = Math.max(clampMargin, Math.min(y, height - clampMargin));
    return Math.max(0, Math.min(1 - (y - padding[0]) / h, 1));
  }
}

Evaluation.propTypes = propTypes;
Evaluation.defaultProps = defaultProps;
