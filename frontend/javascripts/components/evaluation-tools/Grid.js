import React from "react";
import objectAssign from "object-assign";

function range (from, to, step) {
  const t = [];
  for (let i=from; i<to; i+=step)
    t.push(i);
  return t;
}

function sameShadowObject (a, b) {
  const aKeys = Object.keys(a);
  const bKeys = Object.keys(b);
  if (aKeys.length !== bKeys.length) return false;
  for (let i in a) {
    if (a[i] !== b[i])
      return false;
  }
  return true;
}

function interp (a, b, x) {
  return a * (1-x) + b * x;
}

export default class Grid extends React.Component {
  constructor (props) {
    super(props);
    this.x = this.x.bind(this);
    this.y = this.y.bind(this);
  }

  x (value) {
    return Math.round(interp(this.props.xFrom, this.props.xTo, value));
  }

  y (value) {
    return Math.round(interp(this.props.yFrom, this.props.yTo, value));
  }

  gridX (div) {
    var step = 1 / div;
    return range(0, 1, step).map(this.x);
  }

  gridY (div) {
    var step = 1 / div;
    return range(0, 1, step).map(this.y);
  }

  centerGridX () {
    return range(0.5, 1, 0.5).map(this.x);
  }

  centerGridY () {
    return range(0.5, 1, 0.5).map(this.y);
  }

  shouldComponentUpdate(nextProps) {
    const {
      xFrom,
      yFrom,
      xTo,
      yTo
    } = this.props;
    if (nextProps.xFrom !== xFrom || nextProps.yFrom !== yFrom || nextProps.xTo !== xTo || nextProps.yTo !== yTo) {
      return true
    }

    const {
      background,
      gridColor,
      textStyle
    } = this.props;
    return nextProps.background !== background ||
      nextProps.gridColor !== gridColor ||
      !sameShadowObject(nextProps.textStyle, textStyle);
  }

  render() {
    const { x, y } = this;
    const {
      background,
      gridColor,
      textStyle
    } = this.props;

    const sx = x(0);
    const sy = y(0);
    const ex = x(1);
    const ey = y(1);

    const xhalf = this.centerGridX();
    const yhalf = this.centerGridY();
    const xtenth = this.gridX(10);
    const ytenth = this.gridY(10);

    const gridbg =
      `M${sx},${sy} L${sx},${ey} L${ex},${ey} L${ex},${sy} Z`;

    const tenth =
      xtenth
      .map(xp => `M${xp},${sy} L${xp},${ey}`)
      .concat(ytenth.map(yp => `M${sx},${yp} L${ex},${yp}`))
      .join(" ");

    const half =
      xhalf
      .map(xp => `M${xp},${sy} L${xp},${ey}`)
      .concat(yhalf.map(yp => `M${sx},${yp} L${ex},${yp}`))
      .join(" ");

    const ticksLeft =
      ytenth.map( (yp, i) => {
        const w = 3 + (i % 5 === 0 ? 2 : 0);
        return `M${sx},${yp} L${sx-w},${yp}`;
      }).join(" ");

    const ticksBottom =
      xtenth.map((xp, i) => {
        const h = 3 + (i % 5 === 0 ? 2 : 0);
        return `M${xp},${sy} L${xp},${sy+h}`;
      }).join(" ");

    return <g>
      <path
        fill={background}
        d={gridbg} />
      <path
        strokeWidth="1px"
        stroke={gridColor}
        d={tenth} />
      <path
        strokeWidth="2px"
        stroke={gridColor}
        d={half} />
      <text
        style={objectAssign({ textAnchor: "middle", writingMode: "tb" }, textStyle)}
        x={this.x(0)-5}
        y={this.y(0.5)}
      >すっきり</text>
      <text
        style={objectAssign({ textAnchor: "middle", writingMode: "tb" }, textStyle)}
        x={this.x(1)+5}
        y={this.y(0.5)}
      >濃厚</text>
      <text
        style={objectAssign({ dominantBaseline: "text-before-edge" }, textStyle)}
        textAnchor="middle"
        x={this.x(0.5)}
        y={this.y(1)-10}>甘口</text>
      <text
        style={objectAssign({ dominantBaseline: "text-before-edge" }, textStyle)}
        textAnchor="middle"
        x={this.x(0.5)}
        y={this.y(0)}>辛口</text>
    </g>;
  }
}
