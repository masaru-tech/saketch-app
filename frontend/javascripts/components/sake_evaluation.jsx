import ModalHeader from './modal_header.jsx'
import Evaluation from "./evaluation-tools/evaluation.jsx";
import IconRating from "./icon-rating.jsx"

class SakeEvaluationList extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      value: [ 0.5, 0.5 ],
      rating: 0,
      temperature: 15
    };
    this.onChange = this.onChange.bind(this);
    this.onRatingChange = this.onRatingChange.bind(this);
  }

  onChange(value) {
    this.setState({ value });
  }

  onRatingChange(rating) {
    this.setState({rating: rating});
  }

  render() {
    var {
      value
    } = this.state;

    return (
      <ul className="list range-list">
        <li className="list__header">お気に入り度</li>
        <li className="list__item" style={{marginTop: "10px"}}>
          <IconRating toggledClassName="icomoon-heart heart-rating active"
                      untoggledClassName="icomoon-heart heart-rating"
                      onChange={this.onRatingChange} />
        </li>

        <li className="list__header">温度</li>
        <li className="list__item">
          <ons-row>
            <ons-col className="range-wrapper">
              <select valueLink={this.linkState("temperature")}>
                <option value="5">雪冷え(5℃)</option>
                <option value="10">花冷え(10℃)</option>
                <option value="15">涼冷え(15℃)</option>
                <option value="20">冷や(20℃)</option>
                <option value="30">日向燗(30℃)</option>
                <option value="35">人肌燗(35℃)</option>
                <option value="40">ぬる燗(40℃)</option>
                <option value="45">上燗(45℃)</option>
                <option value="50">熱燗(50℃)</option>
                <option value="55">飛び切り燗(55℃)</option>
              </select>
            </ons-col>
          </ons-row>
        </li>

        <li className="list__header">味</li>
        <li className="list__item">
          <div>
            <Evaluation
              value={value}
              onChange={this.onChange}
              style={{
                display: "inline-block",
                margin: "5px"
              }}
              handleStroke={3}
              handleRadius={10}>
            </Evaluation>
          </div>
        </li>
      </ul>
    );
  }
}
SakeEvaluationList.prototype.linkState = React.addons.LinkedStateMixin.linkState

class SakeEvaluationContent extends React.Component {
  constructor (props) {
    super(props);
    this.registerHandler = this.registerHandler.bind(this);
  }

  registerHandler() {
    var user = Parse.User.current();
    var evaluation = new Parse.Object("Evaluation");
    evaluation.set("sakeId", this.props.params.id);
    evaluation.set("rating", this.refs.SakeEvaluationList.state.rating);
    evaluation.set("temperature", Number(this.refs.SakeEvaluationList.state.temperature));
    evaluation.set("evaluation", this.refs.SakeEvaluationList.state.value);
    evaluation.set("user", user);
    var acl = new Parse.ACL();
    acl.setPublicReadAccess(true);
    acl.setWriteAccess(user.id, true);
    evaluation.setACL(acl);
    return evaluation.save();
  }

  render() {
    return (
      <div className="page">
        <ModalHeader title="評価" registerHandler={this.registerHandler} />
        <div style={{paddingTop: "44px"}}>
          <SakeEvaluationList ref="SakeEvaluationList" />
        </div>
      </div>
    );
  }
}

export default SakeEvaluationContent
