import Header from './header.jsx';

class NewUser extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      email: "",
      username: "",
      password: "",
    };
  }

  onClick() {
    var self = this;
    var user = new Parse.User();
    user.set("username", this.state.username);
    user.set("password", this.state.password);
    user.set("email", this.state.email);
    user.signUp().then(function() {
      user.setACL(new Parse.ACL(user));
      user.save();
      var query = new Parse.Query(Parse.Role);
      query.equalTo("name", "saker");
      return query.first();
    }).then(function(role) {
      var relation = role.relation("users");
      relation.add(user);
      return role.save();
    }).then(function(){
      Parse.User.logOut();
      self.context.router.transitionTo('/users/sign_in');
    }, function(error) {
      console.log(error);
    });
  }

  render() {
    return (
      <div className="page">
        <Header title="新規ユーザ登録" back="true" />
        <div style={{paddingTop: "44px"}}>
          <div className="login-form">
            <input type="text" className="text-input--underbar" name="username" placeholder="ユーザ名" valueLink={this.linkState('username')} />
            <input type="email" className="text-input--underbar" name="email" placeholder="メールアドレス" valueLink={this.linkState('email')} />

            <input type="password" className="text-input--underbar" name="password" placeholder="パスワード" valueLink={this.linkState('password')} />
            <div className="lucent">
              <p className="note">パスワード - 6文字以上</p>
            </div>

            <div className="button-area">
              <button className="button login-button button--large" onClick={this.onClick.bind(this)}>新規登録</button>
            </div>

          </div>
        </div>
      </div>
    );
  }
}

NewUser.prototype.linkState = React.addons.LinkedStateMixin.linkState

NewUser.contextTypes = {
  router: function contextType() {
    return React.PropTypes.func.isRequired;
  }
};

export default NewUser
