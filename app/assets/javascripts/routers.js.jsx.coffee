Router = ReactRouter
DefaultRoute = Router.DefaultRoute
Route = Router.Route
RouteHandler = Router.RouteHandler

App = React.createClass
  render: ->
    `<div>
      <RouteHandler {...this.props}/>
    </div>`

@AppRouter =
  `<Route path="/" handler={App}>
    <Route path="users">
      <Route name="new" path="new" handler={NewUser} />
      <Route name="sign_in" path="sign_in" handler={LoginContent} />
      <Route path="password">
        <Route name="reset_password" path="new" handler={ResetPassword} />
      </Route>
    </Route>
    <Route path="sakes">
      <Route name="index" path="index" handler={SakeListContent} />
      <Route name="show" path=":id/show" handler={SakeDetailContent} />
      <Route name="evaluation" path=":id/evaluation" handler={SakeEvaluationContent} />
    </Route>
  </Route>`

# window.onload = ->
#  ReactRouter.run(@AppRouter, Router.HistoryLocation, (Handler)->
#    ReactDOM.render(`<Handler/>`, document.getElementById("content"))
#  )
