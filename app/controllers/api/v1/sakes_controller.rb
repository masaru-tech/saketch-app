class Api::V1::SakesController < ApiController
  def index
    sakes = Sake.all.map do |sake|
      {id: sake.id.to_s, name: sake.name, yomi: sake.yomi.to_s, kuramoto: sake.kuramoto.to_s, prefecture: sake.prefecture.to_s}
    end

    render :json => {sakes: sakes}.to_json
  end

  def show
    sake = Sake.find(params[:id])
    render :json => sake.to_h.to_json
  end
end
