class SakesController < ApplicationController
  def index
  end

  def show
    @sake = Sake.find(params[:id]).to_h
  end
end
