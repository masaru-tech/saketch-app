class Sake
  include Mongoid::Document

  field :nm,  type: String,  :as => :name
  field :ym,  type: String,  :as => :yomi
  field :cg,  type: Array,   :as => :category
  field :pf,  type: String,  :as => :prefecture
  field :km,  type: String,  :as => :kuramoto
  field :rr,  type: Array,   :as => :raw_rice
  field :cr,  type: Range,   :as => :cleaning_rate
  field :nd,  type: Range,   :as => :nihonsyu_degree
  field :ad,  type: Float,   :as => :amino_degree
  field :acd, type: Float,   :as => :acidity
  field :ac,  type: Range,   :as => :alcohol_content
  field :ue,  type: String,  :as => :use_east
  field :nt,  type: String,  :as => :note

  has_many :sake_evaluations

  def to_h
    {
      id: self.id.to_s,
      name: self.name,
      yomi: self.yomi,
      category: self.category || [],
      prefecture: self.prefecture,
      kuramoto: self.kuramoto,
      raw_rice: self.raw_rice || [],
      cleaning_rate: {
        min: self.cleaning_rate.try(:min),
        max: self.cleaning_rate.try(:max)
      },
      nihonsyu_degree: {
        min: self.nihonsyu_degree.try(:min),
        max: self.nihonsyu_degree.try(:max)
      },
      amino_degree: self.amino_degree,
      acidity: self.acidity,
      alcohol_content: {
        min: self.alcohol_content.try(:min),
        max: self.alcohol_content.try(:max)
      },
      use_east: self.use_east,
      note: self.note
    }
  end
end
