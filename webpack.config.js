path    = require("path")
webpack = require("webpack")

module.exports = {
  progress: true,
  entry: {
    "components": "./frontend/javascripts/components.js"
  },
  output: {
    filename: "[name].bundle.js"
  },
  externals: {
    "react": "React",
    "react-dom": "ReactDOM"
  },
  devtool: '#inline-source-map',
  module: {
    loaders: [
      {
        loader: "babel-loader",

        // Skip any files outside of your project's `frontend` directory
        include: [
          path.resolve(__dirname, "frontend"),
        ],

        exclude: [
          path.resolve(__dirname, "node_modules"),
        ],

        // Only run `.js` and `.jsx` files through Babel
        test: /\.jsx?$/,

        // Options to configure babel with
        query: {
          plugins: ['transform-runtime'],
          presets: ['es2015', 'react'],
        }
      },
    ]
  }
};
